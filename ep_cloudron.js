/* jslint node:true */
/* jslint esversion: 8 */

'use strict';

const async = require('async'),
    bodyParser = require('body-parser'),
    eejs = require('ep_etherpad-lite/node/eejs/'),
    authorManager = require('ep_etherpad-lite/node/db/AuthorManager'),
    padManager = require('ep_etherpad-lite/node/db/PadManager'),
    fs = require('fs'),
    ldapjs = require('ldapjs');

const PRIVATE_PAD_PREFIX = 'private_';

const LDAP_URL = process.env.CLOUDRON_LDAP_URL;
const LDAP_BIND_DN = process.env.CLOUDRON_LDAP_BIND_DN;
const LDAP_BIND_PASSWORD = process.env.CLOUDRON_LDAP_BIND_PASSWORD;
const LDAP_USERS_BASE_DN = process.env.CLOUDRON_LDAP_USERS_BASE_DN;

const USE_LDAP = LDAP_URL && LDAP_BIND_DN && LDAP_BIND_PASSWORD && LDAP_USERS_BASE_DN;

var settings = {};

exports.eejsBlock_indexWrapper = function (hook_name, context) {
    if (settings.requireAuthentication) return;

    // if we don't require authentication the index page should have a login button
    context.content += [
    '<style>',
    '     .login-button {',
    '        position: absolute;',
    '        right: 10px;',
    '        top: 10px;',
    '        margin: 0 auto;',
    '        text-align: center;',
    '        text-shadow: none;',
    '        padding: 0 20px;',
    '        font-size: 23px;',
    '        line-height: 1.8;',
    '        color: #64d29b;',
    '        background: #586a69;',
    '        border-radius: 3px;',
    '        box-shadow: none;',
    '        text-decoration: none;',
    '        border: none;',
    '        display: inline-block;',
    '     }',
    '</style>',
    '<a class="login-button" href="/portal">Login</a>',
    ].join('\n');
};

exports.loadSettings = function (hook_name, context) {
    settings = context.settings;

    // ensure we have admins array set
    if (typeof settings.ep_cloudron !== 'object') settings.ep_cloudron = {};
    if (!Array.isArray(settings.ep_cloudron.admins)) settings.ep_cloudron.admins = [];
};

function handleAuthFailure(req, res) {
    let redirect = req.query.redirect  || req.path || '';

    // prevent redirecting to non-existing GET login route
    if (redirect === '/ep_cloudron/login') redirect = '';

    console.debug(`ep_cloudron.handleAuthFailure: ${redirect}`);

    const render_args = {
        style: eejs.require('ep_cloudron/templates/style.ejs', {}),
        invalidCredentials: !!req.session.invalidCredentials,
        redirect
    };

    res.send(eejs.require('ep_cloudron/templates/login.ejs', render_args));
}

exports.expressConfigure = function (hook_name, args, cb) {
    // Cloudron app healthcheck route handler
    args.app.use(function (req, res, next) {
        if (req.path === '/healthcheck') return res.sendStatus(200);
        next();
    });

    function portal(req, res) {
        console.debug('ep_cloudron get portal');

        if (!req.session.user) {
            const render_args = {
                style: eejs.require('ep_cloudron/templates/style.ejs', {}),
                invalidCredentials: false,
                redirect: ''
            };

            res.send(eejs.require('ep_cloudron/templates/login.ejs', render_args));
            return;
        }

        getDocuments(function (pads) {
            console.debug('ep_cloudron get /: pads', pads);

            var render_args = {
                style: eejs.require('ep_cloudron/templates/style.ejs', {}),
                documents: pads,
                user: req.session.user || null
            };

            res.send(eejs.require('ep_cloudron/templates/index.ejs', render_args));
        });
    }

    // this overwrites the built-in index.html
    args.app.get('/', function (req, res, next) {
        if (!settings.requireAuthentication) return next();

        console.debug('ep_cloudron get /');

        res.redirect('/portal');
    });

    // if requireAuthentication is false, we stil want a login portal
    args.app.get('/portal', portal);

    // the following routes are prefixed with /admin only to work well with the built-in authorization flow
    args.app.get('/ep_cloudron/logout', function (req, res) {
        delete req.session.user;
        res.clearCookie('express_sid');
        res.redirect(req.query.redirect || '/');
    });

    args.app.post('/ep_cloudron/login', function (req, res) {
        console.debug('ep_cloudron post /ep_cloudron/login');

        if (settings.requireAuthentication) return res.redirect(req.query.redirect || '/portal');

        console.debug('ep_cloudron post /ep_cloudron/login require auth');

        // if we land here we don't require public authentication but still want to log the user in
        exports.authenticate('authenticate', { req, res }, function (result) {
            if (result[0]) return res.redirect(req.query.redirect || '/portal');

            handleAuthFailure(req, res);
        });
    });

    cb();
};


// https://github.com/ether/etherpad-lite/blob/develop/doc/api/hooks_server-side.md#preauthorize
exports.preAuthorize = (hookName, context, cb) => {
      if (context.req.path === '/' || context.req.path === '/healthcheck') return cb([true]);

      return cb([]);
};

// Auth flow from webaccess.js:
// https://github.com/ether/etherpad-lite/blob/develop/src/node/hooks/express/webaccess.js (checkAccess)
//    preAuthorize(function (ok) { // static paths are harcoded and authorized
//      if (ok) return next();
//      authorize(function (ok) {
//       if (ok) return next();
//       authenticate(function (ok) {
//         if (!ok) return failure();
//         authorize(function (ok) {
//           if (ok) return next();
//           failure();
//         });
//       });
//     });
//  });

// this is reused in POST login if requireAuthentication is false
exports.authenticate = function (hook_name, context, cb) {
    console.debug('ep_cloudron.authenticate');

    if (context.req.headers['content-type'] !== 'application/x-www-form-urlencoded') {
        console.debug('ep_cloudron.authenticate: failed authentication no auth form data');
        return cb([false]);
    }

    bodyParser.urlencoded({ extended: true })(context.req, context.res, function () {
        var username = context.req.body.username;
        var password = context.req.body.password;

        if (!username || !password) {
            console.debug('ep_cloudron.authenticate: failed authentication empty username or password');
            return cb([false]);
        }

        console.debug('ep_cloudron.authenticate: ', username, password ? '******' : '');

        if (!USE_LDAP) {
            if (settings.users[username] != undefined && settings.users[username].password === password) {
                // clear any previous invalid credentials
                context.req.session.invalidCredentials = false;

                settings.users[username].username = username;
                context.req.session.user = settings.users[username];

                return cb([true]);
            }

            context.req.session.invalidCredentials = true;

            return cb([false]);
        }

        var ldapClient = ldapjs.createClient({ url: LDAP_URL });
        ldapClient.on('error', function (error) {
            console.error('LDAP error', error);
            return cb([false]);
        });

        ldapClient.bind(LDAP_BIND_DN, LDAP_BIND_PASSWORD, function (error) {
            if (error) {
                console.debug('ep_cloudron.authenticate: failed authentication. Unable to bind.', error);
                return cb([false]);
            }

            ldapClient.search(LDAP_USERS_BASE_DN, { filter: '(|(uid=' + username + ')(mail=' + username + ')(username=' + username + ')(sAMAccountName=' + username + '))' }, function (error, result) {
                if (error) {
                    console.debug('ep_cloudron.authenticate: failed authentication. Unable to search.', error);
                    return cb([false]);
                }

                var items = [];

                result.on('searchEntry', function(entry) {
                    items.push(entry.object);
                });

                result.on('error', function (error) {
                    console.debug('ep_cloudron.authenticate: failed authentication. Error listing results.', error);
                    cb([false]);
                });

                result.on('end', function (result) {
                    if (result.status !== 0) {
                        console.debug('ep_cloudron.authenticate: failed authentication. non-zero status from LDAP search:', result.status);
                        return cb([false]);
                    }

                    if (items.length === 0) {
                        console.debug('ep_cloudron.authenticate: failed authentication. No LDAP entries found');

                        context.req.session.invalidCredentials = true;

                        return cb([false]);
                    }

                    var ldapDn = 'cn=' + items[0].username + ',' + LDAP_USERS_BASE_DN;

                    ldapClient.bind(ldapDn, password, function (error) {
                        if (error) {
                            console.debug('ep_cloudron.authenticate: failed authentication. Wrong password.');

                            context.req.session.invalidCredentials = true;

                            return cb([false]);
                        }

                        // clear any previous invalid credentials
                        context.req.session.invalidCredentials = false;

                        // Check if an admin was already configured, otherwise make this user the first admin
                        if (settings.ep_cloudron.admins.length === 0) {
                            settings.ep_cloudron.admins.push(items[0].username);

                            try {
                                var settingsFilename = '/app/data/settings.json';
                                console.log('ep_cloudron.authenticate: Create settings file', settingsFilename, 'for first user', items[0].username, 'as admin.');
                                fs.writeFileSync(settingsFilename, JSON.stringify({ ep_cloudron: { admins: settings.ep_cloudron.admins }}, false, 2), 'utf8');
                            } catch (e) {
                                console.error('ep_cloudron.authenticate: Unable to save settings.json', e.message);
                            }
                        }

                        // User authenticated, save off some information needed for authorization
                        context.req.session.user = {
                            username: items[0].username,
                            display_name: items[0].displayname,
                            is_admin: settings.ep_cloudron.admins.indexOf(items[0].username) !== -1
                        };

                        console.debug('ep_cloudron.authenticate: successful authentication', context.req.session.user);

                        cb([true]);
                    });
                });
            });
        });
    });
};

// https://github.com/ether/etherpad-lite/blob/develop/doc/api/hooks_server-side.md#authnfailure
exports.authnFailure = function (hook_name, context, cb) {
    handleAuthFailure(context.req, context.res);

    // signal that we have handled it
    cb([true]);
};

exports.handleMessage = function (hook_name, context, cb) {
    // skip if we don't have any information to set
    var session = context.client.client.request.session;
    if (!session || !session.user || !session.user.display_name) return cb();

    authorManager.getAuthor4Token(context.message.token).then(function (author) {
        authorManager.setAuthorName(author, context.client.client.request.session.user.display_name);
        cb();
    }).catch(function (error) {
        console.error('handleMessage: could not get authorid for token %s', context.message.token, error);
        cb();
    });
};

function getDocuments(callback) {
    var documents = [];

    console.debug('ep_cloudron.getDocuments');

    padManager.listAllPads().then(function (listResult) {
        console.debug('ep_cloudron.getDocuments:', listResult);

        var padNames = listResult.padIDs;

        if (padNames.length === 0) return callback([]);

        async.each(padNames, function (padName, callback) {
            console.debug('ep_cloudron.getDocuments: get pad', padName);
            // ignore private pads
            if (padName.indexOf(PRIVATE_PAD_PREFIX) === 0) return callback();

            padManager.getPad(padName).then(function (pad) {
                console.debug('ep_cloudron.getDocuments: got pad', pad);
                if (!pad) return callback(null);

                // ignore pads without any changes
                if (pad.head === 0) return callback();

                pad.getLastEdit().then(function (time) {
                    console.debug('ep_cloudron.getDocuments: las edit time', time);
                    var d = new Date(time);

                    documents.push({
                        name: padName,
                        lastRevision: pad.head,
                        lastAccess: d.toLocaleTimeString() + ' ' + d.toLocaleDateString()
                    });

                    callback();
                }).catch(function (error) { callback(error); });
            }).catch(function (error) { callback(error); });
        }, function (error) {
            if (error) console.error(error);

            callback(documents);
        });
    }).catch(function (error) {
        console.error(error);

        callback(documents);
    });
}
